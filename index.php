<?php

$search_words = ['php', 'html', 'интернет', 'Web'];

$search_strings = [

    'Интернет - большая сеть компьютеров, которые могут взаимодействовать друг с другом.',

    'PHP - это распространенный язык программирования с открытым исходным кодом.',

    'PHP сконструирован специально для ведения Web - разработок и его код может внедряться непосредственно в HTML',

];

function start($search_words, $search_strings)
{
    $a = '';
    foreach ($search_strings as $key => $value)
    {
        $numberKey = $key+1;
        $a .= '<br>' . "В предложении #$numberKey есть слова:";
        foreach ($search_words as $search_word)
        {
            $a .= ' ';
            preg_match_all("/$search_word/ui", "$value", $matches);
            foreach ($matches as $text)
            {
                $a .= $text[0];
            }
        }
    }

    return $a;
}


print_r(start($search_words, $search_strings));
